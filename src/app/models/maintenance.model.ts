export interface Maintenance {
    id: number;
    reason: string;
    startPeriodMaintenance: string;
    endPeriodMaintenance: string;
}
