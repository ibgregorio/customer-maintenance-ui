import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Maintenance } from '../models/maintenance.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MaintenanceService {

  constructor(public http: HttpClient) { }

  listMaintenances() : Observable<Maintenance[]> {
    return this.http.get<Maintenance[]>(`${environment.baseUrl}/maintenances`);
  }

  saveMaintenance(newMaintenance: Maintenance) {
    return this.http.post(
        `${environment.baseUrl}/maintenances`,
        newMaintenance,
        {
            observe: 'response',
            responseType: 'text'
        }
    );
  }

  updateMaintenance(updatedMaintenance : Maintenance) {
    return this.http.patch(
        `${environment.baseUrl}/maintenances/${updatedMaintenance.id}`,
        updatedMaintenance,
        {
            observe: 'response',
            responseType: 'text'
        }
    );
  } 

  removeMaintenance(idMaintenance: string) {
    return this.http.delete(
        `${environment.baseUrl}/maintenances/${idMaintenance}`,
        {
            observe: 'response',
            responseType: 'text'
        }
    );
  } 

}
