import { Component, OnInit } from '@angular/core';
import { Maintenance } from 'src/app/models/maintenance.model';
import { MaintenanceService } from 'src/app/services/maintenance.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  maintenances: Maintenance[] = [];

  constructor(private maintenanceService: MaintenanceService) { }

  ngOnInit(): void {
    this.maintenanceService.listMaintenances()
            .subscribe(response => {
                this.maintenances = response;
            },
            error => {});
  }

}
