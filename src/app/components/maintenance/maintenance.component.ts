import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConfirmationService, MessageService } from 'primeng-lts/api';
import { Maintenance } from 'src/app/models/maintenance.model';
import { MaintenanceService } from 'src/app/services/maintenance.service';

@Component({
  selector: 'app-maintenance',
  templateUrl: './maintenance.component.html',
  styleUrls: ['./maintenance.component.scss']
})
export class MaintenanceComponent implements OnInit {

  showMaintenanceDialog: boolean = false;

  formScheduleMaintenance: FormGroup;

  maintenances: Maintenance[] = [];

  selectedMaintenance?: Maintenance;

  minStartDate = new Date();
  minEndDate = new Date();

  constructor(private maintenanceService: MaintenanceService,
              private formBuilder: FormBuilder,
              private messageService: MessageService,
              private confirmationService: ConfirmationService) {
      
      this.formScheduleMaintenance = this.formBuilder.group({
        reason: ['', []],
        startPeriodMaintenance: [null, Validators.required],
        endPeriodMaintenance: [null , Validators.required],
      });
  }

  ngOnInit(): void {
    this.loadScheduledMaintenances();
  }

  submitMaintenance() {
    if (this.formScheduleMaintenance.invalid) {
      return;
    }

    if (this.selectedMaintenance) {
      this.updateMaintenance(this.selectedMaintenance);
    } else {
      this.saveMaintenance();
    }
    
  }

  editMaintenance(maintenance: Maintenance) {
    if (maintenance) {
      this.selectedMaintenance = maintenance;

      this.formScheduleMaintenance.controls['reason'].setValue(this.selectedMaintenance.reason);
      this.formScheduleMaintenance.controls['startPeriodMaintenance'].setValue(new Date(this.selectedMaintenance.startPeriodMaintenance));
      this.formScheduleMaintenance.controls['endPeriodMaintenance'].setValue(new Date(this.selectedMaintenance.endPeriodMaintenance));
      
      this.openMaintenanceDialog();
    }
  }

  openMaintenanceDialog() {
    this.showMaintenanceDialog = true;
  }
  
  closeMaintenanceDialog() {
    this.formScheduleMaintenance.reset();
    this.showMaintenanceDialog = false;
    this.minEndDate = new Date();
  }

  confirmMaintenanceDeletion(maintenance: Maintenance) {
    let selectedMaintenance = maintenance;

    this.confirmationService.confirm({
        message: 'Do you want to delete this scheduled maintenance?',
        header: 'Delete Scheduled Maintenance',
        icon: 'pi pi-question-circle',
        accept: () => {
            this.deleteMaintenance(selectedMaintenance);
        }
    });
  }

  setMinEndDate() {
    let selectedStartDate = this.formScheduleMaintenance.get('startPeriodMaintenance')?.value;

    this.minEndDate = selectedStartDate;

    if (selectedStartDate > this.formScheduleMaintenance.get('endPeriodMaintenance')?.value) {
      this.formScheduleMaintenance.get('endPeriodMaintenance')?.reset();
    }
  }

  private loadScheduledMaintenances() {
    this.maintenanceService.listMaintenances()
            .subscribe(response => {
                this.maintenances = response;
            },
            error => {});
  }

  private saveMaintenance() {
    this.maintenanceService.saveMaintenance(this.formScheduleMaintenance.value)
      .subscribe(() => {
        this.messageService.add({
          severity: 'success', 
          summary: 'Your maintenance has been scheduled successfully'
        });

        this.loadScheduledMaintenances();
        this.closeMaintenanceDialog();
      },
      (error: any) => {
        let errorReturned = JSON.parse(error.error);

        this.messageService.add({
          severity: 'error', 
          summary: errorReturned.message
        });
      });
  }

  private updateMaintenance(selectedMaintenance: Maintenance) {
    selectedMaintenance = Object.assign(selectedMaintenance, this.formScheduleMaintenance.value);
    
    this.maintenanceService.updateMaintenance(selectedMaintenance)
      .subscribe(() => {
        this.messageService.add({
          severity: 'success', 
          summary: 'Your maintenance has been updated'
        });

        this.selectedMaintenance = undefined;
        this.loadScheduledMaintenances();
        this.closeMaintenanceDialog();
      },
      (error: any) => {
        let errorReturned = JSON.parse(error.error);

        this.messageService.add({
          severity: 'error', 
          summary: errorReturned.message
        });
      });
  }

  private deleteMaintenance(maintenance: Maintenance) {
    this.maintenanceService.removeMaintenance(maintenance.id.toString())
      .subscribe(() => {
        this.messageService.add({
          severity: 'success', 
          summary: 'Your scheduled maintenance has been deleted'
        });

        this.loadScheduledMaintenances();
      },
      (error: any) => {
        let errorReturned = JSON.parse(error.error);

        this.messageService.add({
          severity: 'error', 
          summary: errorReturned.message
        });
      });
  }
}
